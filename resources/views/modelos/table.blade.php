<div class="table-responsive">
    <table class="table" id="modelos-table">
        <thead>
            <tr>
                <th>@lang('models/modelos.fields.nombre')</th>
                <th>@lang('models/modelos.fields.marca_id')</th>
                <th>@lang('models/modelos.fields.descripcion')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
            @foreach($modelos as $modelos)
            <tr>
             <td>{{ $modelos->nombre }}</td>
             <td>{{ $modelos->marca->nombre }}</td>
             <td>{{ $modelos->descripcion }}</td>
             <td class=" text-center">
                 {!! Form::open(['route' => ['modelos.destroy', $modelos->id], 'method' => 'delete']) !!}
                 <div class='btn-group'>
                     <a href="{!! route('modelos.show', [$modelos->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                     <a href="{!! route('modelos.edit', [$modelos->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                     {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                 </div>
                 {!! Form::close() !!}
             </td>
         </tr>
         
         @endforeach
     </tbody>
 </table>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>


<!--Data Table-->
<script type="text/javascript"  src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js" defer></script>


<script>
   $(document).ready( function () {

    $('#modelos-table').DataTable();
} );
</script>