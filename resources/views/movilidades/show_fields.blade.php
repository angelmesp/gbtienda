<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', __('models/movilidades.fields.nombre').':') !!}
    <p>{{ $movilidades->nombre }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', __('models/movilidades.fields.descripcion').':') !!}
    <p>{{ $movilidades->descripcion }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/movilidades.fields.created_at').':') !!}
    <p>{{ $movilidades->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/movilidades.fields.updated_at').':') !!}
    <p>{{ $movilidades->updated_at }}</p>
</div>

