<div class="table-responsive">
    <table class="table" id="marcas-table">
        <thead>
            <tr>
                <th>@lang('models/marcas.fields.nombre')</th>
        <th>@lang('models/marcas.fields.descripcion')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($marcas as $marcas)
            <tr>
                       <td>{{ $marcas->nombre }}</td>
            <td>{{ $marcas->descripcion }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['marcas.destroy', $marcas->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('marcas.show', [$marcas->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('marcas.edit', [$marcas->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
