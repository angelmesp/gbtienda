<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class rep_plantillas
 * @package App\Models
 * @version October 29, 2021, 12:31 pm UTC
 *
 * @property string $nombre
 * @property integer $precio
 * @property integer $plantilla_id
 */
class rep_plantillas extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'rep_plantillas';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'precio',
        'plantilla_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'precio' => 'integer',
        'plantilla_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'precio' => 'required',
        'plantilla_id' => 'required'
    ];

    public function plantilla()
    {
        return $this->belongsTo(plantillas::class, 'plantilla_id');
    }

    
}
