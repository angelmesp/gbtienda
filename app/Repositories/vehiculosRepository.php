<?php

namespace App\Repositories;


use App\Models\vehiculos;
use App\Repositories\BaseRepository;
// Uso imagen
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
// uso imagen fin
use \App\Http\Controllers\vehiculosController;

/**
 * Class vehiculosRepository
 * @package App\Repositories
 * @version November 4, 2021, 7:39 pm UTC
*/

class vehiculosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'version_id',
        'inidesarme',
        'preciocompra',
        'imagen',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return vehiculos::class;
    }

    public function createVehiculo(Request $request){
        // Creamos esta funcion para intervenir las imagenes



        $files = $request->file('imagen');
        $imagenes = [];
        foreach ($files as $key => $file) {
         $originalName = $file->getClientOriginalName();
         $extantion = $file->getCLientoriginalExtension();

         $hoy = date("Ymd");  

         $pathimage = $hoy.time().'_'.uniqid().'.'.$extantion;
         $path = 'endesarme/'.$pathimage;
         $img = Image::make($file)->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        })->insert('img/Marca_desarmaduria_autorey_camionetas.png');



         $img->save(public_path($path));
         array_push($imagenes, $pathimage);
     }



        // Intervenimos QRcode
        //$qrcode = $request->file('qrcode');
        //$originalNameqrcode = $qrcode->getClientOriginalName();
        //$extantionqrcode = $qrcode->getCLientoriginalExtension();

     $input = $request->all();
        //$pathqrcode = productosController::qr_generate($input['nombre'],$input['imagen']);

     $imagenes = json_encode($imagenes);
     $input['imagen'] = $imagenes;
        //$input['qrcode'] = $pathqrcode;           

     return $this->create($input);

 }

 public function updateVehiculo(Request $request, $id){
       // return 'editando repuesto updateRepuesto';
        // Creamos esta funcion para intervenir las imagenes
   $files = $request->file('imagen');
        $imagenes = [];
        foreach ($files as $key => $file) {
         $originalName = $file->getClientOriginalName();
         $extantion = $file->getCLientoriginalExtension();

         $hoy = date("Ymd");  

         $pathimage = $hoy.time().'_'.uniqid().'.'.$extantion;
         $path = 'endesarme/'.$pathimage;
         $img = Image::make($file)->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        })->insert('img/Marca_desarmaduria_autorey_camionetas.png');



         $img->save(public_path($path));
         array_push($imagenes, $pathimage);
     }



    $input = $request->all();
        //$pathqrcode = repuestoController::qr_generate($input['nombre'],$input['imagen']);

$imagenes = json_encode($imagenes);
     $input['imagen'] = $imagenes;
        //$input['qrcode'] = $pathqrcode;           

    return $this->update($input, $id);


} 


}