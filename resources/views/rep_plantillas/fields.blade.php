<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', __('models/repPlantillas.fields.nombre').':') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Precio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('precio', __('models/repPlantillas.fields.precio').':') !!}
    {!! Form::text('precio', null, ['class' => 'form-control']) !!}
</div>

<!-- Plantilla Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('plantilla_id', __('models/repPlantillas.fields.plantilla_id').':') !!}
    {!! Form::select('plantilla_id', $plantillaItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('repPlantillas.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
