<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', __('models/repPlantillas.fields.nombre').':') !!}
    <p>{{ $repPlantillas->nombre }}</p>
</div>

<!-- Precio Field -->
<div class="form-group">
    {!! Form::label('precio', __('models/repPlantillas.fields.precio').':') !!}
    <p>{{ $repPlantillas->precio }}</p>
</div>

<!-- Plantilla Id Field -->
<div class="form-group">
    {!! Form::label('plantilla_id', __('models/repPlantillas.fields.plantilla_id').':') !!}
    <p>{{ $repPlantillas->plantilla_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/repPlantillas.fields.created_at').':') !!}
    <p>{{ $repPlantillas->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/repPlantillas.fields.updated_at').':') !!}
    <p>{{ $repPlantillas->updated_at }}</p>
</div>

