<?php

return array (
  'singular' => 'plantillas',
  'plural' => 'plantillas',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'prefijo' => 'Prefijo',
    'descripcion' => 'Descripcion',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
