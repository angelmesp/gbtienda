@extends('layouts.app')
@section('title')
@lang('models/productos.plural')
@endsection
@section('content')
<section class="section">
    <div class="section-header">
        <h1>@lang('models/productos.plural')</h1>
        <div class="section-header-breadcrumb">
            <a href="{{ route('productos.create', ['idvec'=>0])}}" class="btn btn-primary form-btn">@lang('crud.add_new')<i class="fas fa-plus"></i></a>

            {!!  Form::open(['route' => 'imprimir', 'files' => true]) !!}
            <label for="" style="margin-left:10px">Imprimir por fecha
                <input style="display: inline-block;width: 196px;margin-left: 10px;" type="date" class="form-control" name="fecha">
            </label>
            <button type="submit"  class="btn btn-icon icon-left btn-info" style="margin-left: 4px;">
               <i class="fas fa-print"></i> Imprimir
           </button>

           {!! Form::close() !!}


       </div>
   </div>
   <div class="section-body">
     <div class="card">
        <div class="card-body">
            @include('productos.table')
        </div>
    </div>
</div>

</section>
@endsection



