<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\modelos;

/**
 * Class marcas
 * @package App\Models
 * @version October 15, 2021, 2:17 pm UTC
 *
 * @property string $nombre
 * @property string $descripcion
 */
class marcas extends Model
{
   // use SoftDeletes;

    use HasFactory;

    public $table = 'marcas';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required'
    ];


    // Marca tiene muchos modelos
    /*public function modelos()
    {
        return $this->hasMany(related:modelos::class);
    }

    // Relacion con versiones y modelos
    public function versiones()
    {
        return $this->hasManyThrough(related:versiones::class, through:modelos::class);
    }
    */

    //Una marca tiene muchos modelos accedemos $marca->modelos; obtenemos una coleccion
    public function modelos()
    {
        return $this->hasMany(modelos::class, 'marca_id');
    }


    
}
