<?php

return array (
  'singular' => 'Plantilla',
  'plural' => 'Plantillas',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'prefijo' => 'Prefijo',
    'descripcion' => 'Descripcion',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
