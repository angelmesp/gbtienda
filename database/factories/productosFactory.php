<?php

namespace Database\Factories;

use App\Models\productos;
use Illuminate\Database\Eloquent\Factories\Factory;

class productosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = productos::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->word,
        'precio' => $this->faker->randomDigitNotNull,
        'idvehiculo' => $this->faker->randomDigitNotNull,
        'idestado' => $this->faker->randomDigitNotNull,
        'idmovilidad' => $this->faker->randomDigitNotNull,
        'bodega' => $this->faker->word,
        'codrepinter' => $this->faker->word,
        'codrepfab' => $this->faker->word,
        'codigobarra' => $this->faker->randomDigitNotNull,
        'stock' => $this->faker->randomDigitNotNull,
        'imagen' => $this->faker->word,
        'qrcode' => $this->faker->word,
        'descripcion' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
