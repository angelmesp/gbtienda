<div class="table-responsive">
    <table class="table" id="estados-table">
        <thead>
            <tr>
                <th>@lang('models/estados.fields.nombre')</th>
        <th>@lang('models/estados.fields.descripcion')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($estados as $estados)
            <tr>
                       <td>{{ $estados->nombre }}</td>
            <td>{{ $estados->descripcion }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['estados.destroy', $estados->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('estados.show', [$estados->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('estados.edit', [$estados->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
