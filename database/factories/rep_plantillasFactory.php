<?php

namespace Database\Factories;

use App\Models\rep_plantillas;
use Illuminate\Database\Eloquent\Factories\Factory;

class rep_plantillasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = rep_plantillas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->word,
        'precio' => $this->faker->randomDigitNotNull,
        'plantilla_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
