<?php

namespace Database\Factories;

use App\Models\vehiculos;
use Illuminate\Database\Eloquent\Factories\Factory;

class vehiculosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = vehiculos::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->word,
        'version_id' => $this->faker->randomDigitNotNull,
        'inidesarme' => $this->faker->randomDigitNotNull,
        'preciocompra' => $this->faker->randomDigitNotNull,
        'descripcion' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
