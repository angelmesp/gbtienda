<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatemovilidadesRequest;
use App\Http\Requests\UpdatemovilidadesRequest;
use App\Repositories\movilidadesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class movilidadesController extends AppBaseController
{
    /** @var  movilidadesRepository */
    private $movilidadesRepository;

    public function __construct(movilidadesRepository $movilidadesRepo)
    {
        $this->movilidadesRepository = $movilidadesRepo;
    }

    /**
     * Display a listing of the movilidades.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $movilidades = $this->movilidadesRepository->all();

        return view('movilidades.index')
            ->with('movilidades', $movilidades);
    }

    /**
     * Show the form for creating a new movilidades.
     *
     * @return Response
     */
    public function create()
    {
        return view('movilidades.create');
    }

    /**
     * Store a newly created movilidades in storage.
     *
     * @param CreatemovilidadesRequest $request
     *
     * @return Response
     */
    public function store(CreatemovilidadesRequest $request)
    {
        $input = $request->all();

        $movilidades = $this->movilidadesRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/movilidades.singular')]));

        return redirect(route('movilidades.index'));
    }

    /**
     * Display the specified movilidades.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $movilidades = $this->movilidadesRepository->find($id);

        if (empty($movilidades)) {
            Flash::error(__('messages.not_found', ['model' => __('models/movilidades.singular')]));

            return redirect(route('movilidades.index'));
        }

        return view('movilidades.show')->with('movilidades', $movilidades);
    }

    /**
     * Show the form for editing the specified movilidades.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $movilidades = $this->movilidadesRepository->find($id);

        if (empty($movilidades)) {
            Flash::error(__('messages.not_found', ['model' => __('models/movilidades.singular')]));

            return redirect(route('movilidades.index'));
        }

        return view('movilidades.edit')->with('movilidades', $movilidades);
    }

    /**
     * Update the specified movilidades in storage.
     *
     * @param int $id
     * @param UpdatemovilidadesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatemovilidadesRequest $request)
    {
        $movilidades = $this->movilidadesRepository->find($id);

        if (empty($movilidades)) {
            Flash::error(__('messages.not_found', ['model' => __('models/movilidades.singular')]));

            return redirect(route('movilidades.index'));
        }

        $movilidades = $this->movilidadesRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/movilidades.singular')]));

        return redirect(route('movilidades.index'));
    }

    /**
     * Remove the specified movilidades from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
                    $movilidades = $this->movilidadesRepository->find($id);

                    if (empty($movilidades)) {
                        Flash::error(__('messages.not_found', ['model' => __('models/movilidades.singular')]));

                        return redirect(route('movilidades.index'));
                    }

                    $this->movilidadesRepository->delete($id);

                    Flash::success(__('messages.deleted', ['model' => __('models/movilidades.singular')]));

                    return redirect(route('movilidades.index'));
                } catch(\Exception $exception){

                    //dd($exception);
                    //$errormsg = 'No Customer to de!' .$exception->getCode();
                    //return Response::json(['errormsg'=>$errormsg]);
                    return back()->with('msj', 'Existen rejistros asociados');
                   //return redirect(route('vehiculos.index'));
                }
    }
}
