<?php

namespace App\Repositories;

use App\Models\modelos;
use App\Repositories\BaseRepository;

/**
 * Class modelosRepository
 * @package App\Repositories
 * @version October 18, 2021, 9:55 pm UTC
*/

class modelosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'marca_id',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return modelos::class;
    }
}
