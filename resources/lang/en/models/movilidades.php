<?php

return array (
  'singular' => 'Movilidad',
  'plural' => 'Movilidades',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'descripcion' => 'Descripcion',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
