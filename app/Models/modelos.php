<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class modelos
 * @package App\Models
 * @version October 18, 2021, 9:55 pm UTC
 *
 * @property string $nombre
 * @property integer $marca_id
 * @property string $descripcion
 */
class modelos extends Model
{
    //use SoftDeletes;

    use HasFactory;

    public $table = 'modelos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'marca_id',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'marca_id' => 'integer',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'marca_id' => 'required'
    ];

    // Un modelo tiene una marca accedemos $modelos->marca; se obtiene un modelo
    public function marca()
    {
        return $this->belongsTo(marcas::class, 'marca_id');
    }

    
}
