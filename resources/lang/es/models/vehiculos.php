<?php

return array (
  'singular' => 'Vehiculo',
  'plural' => 'Vehiculos',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'version_id' => 'Version',
    'inidesarme' => 'Ini Desarme',
    'preciocompra' => 'P Compra',
    'imagen' => 'Imagen',
    'descripcion' => 'Descripcion',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
