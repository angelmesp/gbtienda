<?php

return array (
  'singular' => 'Marcas',
  'plural' => 'Marcas',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'descripcion' => 'Descripcion',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
