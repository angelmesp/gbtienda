<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', __('models/versiones.fields.nombre').':') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Modelo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modelo_id', __('models/versiones.fields.modelo_id').':') !!}
    {!! Form::select('modelo_id', $modeloItems, null, ['class' => 'form-control']) !!}
</div>

<!-- año inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anoini', __('models/versiones.fields.anoini').':') !!}
    {!! Form::text('anoini', '1990', ['class' => 'form-control']) !!}
</div>

<!-- año fin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anofin', __('models/versiones.fields.anofin').':') !!}
    {!! Form::text('anofin', '2022', ['class' => 'form-control']) !!}
</div>

<!-- Plantilla Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('plantillaId', __('models/versiones.fields.plantillaId').':') !!}
    {!! Form::select('plantillaId', $plantillaItems, null, ['class' => 'form-control']) !!}
</div>
<!-- codVersion Field -->
<div class="form-group col-sm-6" hidden >
    {!! Form::label('codversion', __('models/versiones.fields.codversion').':') !!}
    {!! Form::text('codversion', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', __('models/versiones.fields.descripcion').':') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('versiones.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
