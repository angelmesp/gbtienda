<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createrep_plantillasRequest;
use App\Http\Requests\Updaterep_plantillasRequest;
use App\Repositories\rep_plantillasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class rep_plantillasController extends AppBaseController
{
    /** @var  rep_plantillasRepository */
    private $repPlantillasRepository;

    public function __construct(rep_plantillasRepository $repPlantillasRepo)
    {
        $this->repPlantillasRepository = $repPlantillasRepo;
    }

    /**
     * Display a listing of the rep_plantillas.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $repPlantillas = $this->repPlantillasRepository->all();

        return view('rep_plantillas.index')
            ->with('repPlantillas', $repPlantillas);
    }

    /**
     * Show the form for creating a new rep_plantillas.
     *
     * @return Response
     */
    public function create()
    {
        return view('rep_plantillas.create');
    }

    /**
     * Store a newly created rep_plantillas in storage.
     *
     * @param Createrep_plantillasRequest $request
     *
     * @return Response
     */
    public function store(Createrep_plantillasRequest $request)
    {
        $input = $request->all();

        $repPlantillas = $this->repPlantillasRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/repPlantillas.singular')]));

        return redirect(route('repPlantillas.index'));
    }

    /**
     * Display the specified rep_plantillas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $repPlantillas = $this->repPlantillasRepository->find($id);

        if (empty($repPlantillas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/repPlantillas.singular')]));

            return redirect(route('repPlantillas.index'));
        }

        return view('rep_plantillas.show')->with('repPlantillas', $repPlantillas);
    }

    /**
     * Show the form for editing the specified rep_plantillas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $repPlantillas = $this->repPlantillasRepository->find($id);

        if (empty($repPlantillas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/repPlantillas.singular')]));

            return redirect(route('repPlantillas.index'));
        }

        return view('rep_plantillas.edit')->with('repPlantillas', $repPlantillas);
    }

    /**
     * Update the specified rep_plantillas in storage.
     *
     * @param int $id
     * @param Updaterep_plantillasRequest $request
     *
     * @return Response
     */
    public function update($id, Updaterep_plantillasRequest $request)
    {
        $repPlantillas = $this->repPlantillasRepository->find($id);

        if (empty($repPlantillas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/repPlantillas.singular')]));

            return redirect(route('repPlantillas.index'));
        }

        $repPlantillas = $this->repPlantillasRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/repPlantillas.singular')]));

        return redirect(route('repPlantillas.index'));
    }

    /**
     * Remove the specified rep_plantillas from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $repPlantillas = $this->repPlantillasRepository->find($id);

        if (empty($repPlantillas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/repPlantillas.singular')]));

            return redirect(route('repPlantillas.index'));
        }

        $this->repPlantillasRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/repPlantillas.singular')]));

        return redirect(route('repPlantillas.index'));
    }
}
