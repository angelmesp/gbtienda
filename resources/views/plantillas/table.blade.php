<div class="table-responsive">
    <table class="table" id="plantillas-table">
        <thead>
            <tr>
                <th>@lang('models/plantillas.fields.nombre')</th>
        <th>@lang('models/plantillas.fields.prefijo')</th>
        <th>@lang('models/plantillas.fields.descripcion')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($plantillas as $plantillas)
            <tr>
                       <td>{{ $plantillas->nombre }}</td>
            <td>{{ $plantillas->prefijo }}</td>
            <td>{{ $plantillas->descripcion }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['plantillas.destroy', $plantillas->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('plantillas.show', [$plantillas->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('plantillas.edit', [$plantillas->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
