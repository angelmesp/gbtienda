<div class="table-responsive">
    <table class="table" id="versiones-table">
        <thead>
            <tr>
                <th>@lang('models/versiones.fields.nombre')</th>
        <th>@lang('models/versiones.fields.modelo_id')</th>
        <th>Año</th>
        <th>@lang('models/versiones.fields.plantillaId')</th>
        <th>@lang('models/versiones.fields.codversion')</th>
        <th>@lang('models/versiones.fields.descripcion')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($versiones as $versiones)
            <tr>
                       <td>{{ $versiones->nombre }}</td>
            <td>{{ $versiones->modelo->nombre }}</td>
            <td>{{ $versiones->anoini }} - {{ $versiones->anofin }}</td>
            <td>{{ $versiones->plantilla->nombre }}</td>
            <td>{{ $versiones->codversion }}</td>
            <td>{{ $versiones->descripcion }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['versiones.destroy', $versiones->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('versiones.show', [$versiones->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('versiones.edit', [$versiones->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
