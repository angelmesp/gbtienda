<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', __('models/plantillas.fields.nombre').':') !!}
    <p>{{ $plantillas->nombre }}</p>
</div>

<!-- Prefijo Field -->
<div class="form-group">
    {!! Form::label('prefijo', __('models/plantillas.fields.prefijo').':') !!}
    <p>{{ $plantillas->prefijo }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', __('models/plantillas.fields.descripcion').':') !!}
    <p>{{ $plantillas->descripcion }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/plantillas.fields.created_at').':') !!}
    <p>{{ $plantillas->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/plantillas.fields.updated_at').':') !!}
    <p>{{ $plantillas->updated_at }}</p>
</div>

