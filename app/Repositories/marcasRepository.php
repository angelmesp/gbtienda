<?php

namespace App\Repositories;

use App\Models\marcas;
use App\Repositories\BaseRepository;

/**
 * Class marcasRepository
 * @package App\Repositories
 * @version October 15, 2021, 2:17 pm UTC
*/

class marcasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return marcas::class;
    }
}
