<div class="row">
    <div class="card-header">
        <h4>{{ $productos->nombre }}</h4>
      </div>
    <div class="col-12 col-md-6 col-lg-6">
        <div class="card">
          <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item">
                    <div align="right">
                        {!! Form::open(['route' => ['productos.destroy', $productos->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                            <a href="{!! route('productos.edit', [$productos->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                        {!! Form::close() !!}
                    </div>
                </li>
                <li class="list-group-item">Precio: ${{ number_format($productos->precio, 0, ',', '.') }}  |  Estado: {{ $productos->estado->nombre }}</li>
                <li class="list-group-item">Movilidad: 
                 @switch($productos->movilidad_id)
                        @case(2)
                            <a href='#' class="badge badge-warning">{{ $productos->movilidad->nombre }}</a>                            
                            @break
                        @case(3)
                            <a href='#' class="badge badge-success">{{ $productos->movilidad->nombre }}</a>                          
                          @break
                        @default
                            <a href='#' class="badge badge-light">{{ $productos->movilidad->nombre }}</a>                          
                        @break
                @endswitch
                 | Bodega: {{ $productos->bodega }}</li>
                <li class="list-group-item">Vehiculo: <a href="{!! route('vehiculos.show', [$productos->vehiculo_id]) !!}"> {{ $productos->vehiculo->nombre }}</a> - Versión:{{ $productos->vehiculo_id }} - Modelo:{{ $productos->vehiculo_id }} - Marca:{{ $productos->idvehiculo }}
                </li>
                <li class="list-group-item">Cod interno: {{ $productos->codrepinter }} | Cod fabrica: {{ $productos->codrepfab }} | Cod barra: {{ $productos->codigobarra }}</li>
                <li class="list-group-item">Stock: {{ $productos->stock }}</li>
                <li class="list-group-item">Descripcion: {{ $productos->descripcion }}</li>
                <li class="list-group-item">Creado el {{ $productos->created_at }} y actualizado el{{ $productos->updated_at }}</li>
                <div align="center">
                     <li class="list-group-item"><img height="300" src="{{ asset('qrcodes/' . $productos->qrcode) }}"></li>
                </div>
            </ul>
            </ul>
          </div>
        </div>
        <!-- Div de abajo
        <div class="card">
          <div class="card-header">
            <h4>Disabled</h4>
          </div>
          <div class="card-body">
            <ul class="list-group">
              <li class="list-group-item">Cras justo odio</li>
            </ul>
          </div>
        </div>  -->            
    </div>

    <div class="col-12 col-md-6 col-lg-6">
        <div class="card">
          <div class="card-body">
            <ul class="list-group">
                <li class="list-group-item"><img height="400" src="{{ asset('upload/' . $productos->imagen) }}"></li>
          </div>
        </div>          
    </div>

</div>


<!-- Updated At Field 
<div class="form-group">
    {!! Form::label('updated_at', __('models/productos.fields.updated_at').':') !!}
    <p>{{ $productos->updated_at }}</p>
</div>
-->