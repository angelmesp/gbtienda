<?php

namespace App\Providers;


use App\Models\movilidades;
use App\Models\estados;
use App\Models\vehiculos;
use App\Models\versiones;
use App\Models\plantillas;
use App\Models\Modelos;
use App\Models\marcas;

use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['productos.fields'], function ($view) {
            $movilidadeItems = Movilidades::pluck('nombre','id')->toArray();
            $view->with('movilidadeItems', $movilidadeItems);
        });
        View::composer(['productos.fields'], function ($view) {
            $estadoItems = Estados::pluck('nombre','id')->toArray();
            $view->with('estadoItems', $estadoItems);
        });
        View::composer(['productos.fields'], function ($view) {
            $vehiculoItems = Vehiculos::pluck('nombre','id')->toArray();
            $view->with('vehiculoItems', $vehiculoItems);
        });
        View::composer(['vehiculos.fields'], function ($view) {
            $versioneItems = Versiones::pluck('nombre','id')->toArray();
            $view->with('versioneItems', $versioneItems);
        });
        View::composer(['rep_plantillas.fields'], function ($view) {
            $plantillaItems = Plantillas::pluck('nombre','id')->toArray();
            $view->with('plantillaItems', $plantillaItems);
        });
        View::composer(['versiones.fields'], function ($view) {
            $modeloItems = Modelos::pluck('nombre','id')->toArray();
            $plantillaItems = Plantillas::pluck('nombre','id')->toArray();
            $view->with('modeloItems', $modeloItems)->with('plantillaItems', $plantillaItems);
        });
        View::composer(['modelos.fields'], function ($view) {
            $marcaItems = Marcas::pluck('nombre','id')->toArray();
            $view->with('marcaItems', $marcaItems);
        });
        //
    }
}