<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Repositories\vehiculosRepository;
//use App\Http\Controllers\vehiculosController;
//use App\vehiculos;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $vehiculos = DB::table('vehiculos')->get();
        return view('home')
            ->with('vehiculos', $vehiculos);
        //return view('home');
    }
}
