<div class="table-responsive">
    <table class="table" id="vehiculos-table">
        <thead>
            <tr>
                <th>@lang('models/vehiculos.fields.nombre')</th>
        <th>@lang('models/vehiculos.fields.version_id')</th>
        <th>@lang('models/vehiculos.fields.inidesarme')</th>
        <th>@lang('models/vehiculos.fields.preciocompra')</th>
        <th>@lang('models/vehiculos.fields.imagen')</th>
        
                <th >@lang('crud.action')</th>



            </tr>
        </thead>
        <tbody>
        @foreach($vehiculos as $vehiculos)
            <tr>
            <td><a href="{!! route('vehiculos.show', [$vehiculos->id]) !!}">{{ $vehiculos->nombre }}</a></td>
            <td>{{ $vehiculos->version->nombre }}</td>
            <td>{{ $vehiculos->inidesarme }}</td>
            <td>${{ number_format($vehiculos->preciocompra, 0, ',', '.') }}</td>
            <td><img height="50" src="{{ asset('endesarme/' . json_decode($vehiculos->imagen)[0]) }}"></td>
          
                       <td class=" text-center">
                           {!! Form::open(['route' => ['vehiculos.destroy', $vehiculos->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('vehiculos.show', [$vehiculos->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('vehiculos.edit', [$vehiculos->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                  
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>


<!--Data Table-->
<script type="text/javascript"  src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js" defer></script>


<script>
   $(document).ready( function () {

    $('#vehiculos-table').DataTable();
} );
</script>