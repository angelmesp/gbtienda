<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('home') }}">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
</li>

<li class="nav-item dropdown">
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-archive"></i> <span>Desarme</span></a>
    <ul class="dropdown-menu">
        <li class="{{ Request::is('vehiculos*') ? 'active' : '' }}">
            <a href="{{ route('vehiculos.index') }}"><i class="fa fa-edit"></i><span>@lang('models/vehiculos.plural')</span></a>
         </li>
         </li><li class="{{ Request::is('productos*') ? 'active' : '' }}">
            <a href="{{ route('productos.index') }}"><i class="fa fa-edit"></i><span>@lang('models/productos.plural')</span></a>
        </li>
        <li class="side-menus {{ Request::is('marcas*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('marcas.index') }}"><i class="fas fa-edit"></i><span>Marcas</span></a>
        </li>
        <li>
            <a class="nav-link" href="{{ route('modelos.index') }}"><i class="fas fa-edit"></i><span>Modelos</span></a>
        </li>
        <li class="side-menus {{ Request::is('versiones*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('versiones.index') }}"><i class="fas fa-edit"></i><span>Versiones</span></a>
        </li>
        <li class="side-menus {{ Request::is('estados*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('estados.index') }}"><i class="fas fa-edit"></i><span>Estados</span></a>
        </li>
        <li class="{{ Request::is('movilidades*') ? 'active' : '' }}">
            <a href="{{ route('movilidades.index') }}"><i class="fa fa-edit"></i><span>@lang('models/movilidades.plural')</span></a>
        </li>
        <li class="{{ Request::is('plantillas*') ? 'active' : '' }}">
            <a href="{{ route('plantillas.index') }}"><i class="fa fa-edit"></i><span>@lang('models/plantillas.plural')</span></a>
        </li>

        <li class="{{ Request::is('repPlantillas*') ? 'active' : '' }}">
            <a href="{{ route('repPlantillas.index') }}"><i class="fa fa-edit"></i><span>@lang('models/repPlantillas.plural')</span></a>
        </li>  
            </ul>
        </li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-th-large"></i> <span>Ventas</span></a>
    <ul class="dropdown-menu">
        <li class="side-menus">
            <a class="nav-link" href="#"><i class="fas fa-building"></i><span>Vender</span></a>
        </li>

    </ul>
</li>
<li class="nav-item dropdown">
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-cog"></i> <span>Configuracion</span></a>
    <ul class="dropdown-menu">
        <li class="side-menus">
            <a class="nav-link" href="#"><i class="fas fa-building"></i><span>Sistema</span></a>
        </li>

    </ul>
