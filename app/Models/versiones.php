<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class versiones
 * @package App\Models
 * @version October 18, 2021, 11:41 pm UTC
 *
 * @property string $nombre
 * @property integer $modelo_id
 * @property string $descripcion
 */
class versiones extends Model
{
    //use SoftDeletes;

    use HasFactory;

    public $table = 'versiones';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'modelo_id',
        'plantillaId',
        'descripcion',
        'codversion',
        'anoini',
        'anofin'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'modelo_id' => 'integer',
        'plantillaId' => 'integer',
        'descripcion' => 'string',
        'codversion' => 'string',
        'anoini' => 'integer',
        'anofin' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'modelo_id' => 'required',
        'plantillaId' => 'required',
 
    ];

    public function modelo()
    {
        return $this->belongsTo(modelos::class, 'modelo_id');
    }

    public function plantilla()
    {
        return $this->belongsTo(plantillas::class, 'plantillaId');
    }

    // una version tiene muchos vehiculos
    public function vehiculos()
    {
        return $this->hasMany(vehiculos::class, 'version_id');
    }

    
}
