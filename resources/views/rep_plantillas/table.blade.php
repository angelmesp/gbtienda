<div class="table-responsive">
    <table class="table" id="repPlantillas-table">
        <thead>
            <tr>
                <th>@lang('models/repPlantillas.fields.nombre')</th>
        <th>@lang('models/repPlantillas.fields.precio')</th>
        <th>@lang('models/repPlantillas.fields.plantilla_id')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($repPlantillas as $repPlantillas)
            <tr>
                       <td>{{ $repPlantillas->nombre }}</td>
            <td>{{ $repPlantillas->precio }}</td>
            <td>{{ $repPlantillas->plantilla->nombre }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['repPlantillas.destroy', $repPlantillas->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('repPlantillas.show', [$repPlantillas->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('repPlantillas.edit', [$repPlantillas->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>


<!--Data Table-->
<script type="text/javascript"  src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js" defer></script>


<script>
   $(document).ready( function () {

    $('#repPlantillas-table').DataTable();
} );
</script>