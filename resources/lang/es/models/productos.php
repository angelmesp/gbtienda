<?php

return array (
  'singular' => 'Repuesto',
  'plural' => 'Repuestos',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'precio' => 'Precio',
    'vehiculo_id' => 'Vehiculo',
    'estado_id' => 'Estado',
    'movilidad_id' => 'Movilidad',
    'bodega' => 'Bodega',
    'codrepinter' => 'Cod Interno',
    'codrepfab' => 'Cod fabrica',
    'codigobarra' => 'Cod barra',
    'stock' => 'Stock',
    'imagen' => 'Imagen',
    'qrcode' => 'QR',
    'descripcion' => 'Descripcion',
    'updated_at' => 'Updated At',
  ),
);
