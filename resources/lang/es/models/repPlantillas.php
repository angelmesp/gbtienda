<?php

return array (
  'singular' => 'Repuesto plantilla',
  'plural' => 'Repuestos plantilla',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'precio' => 'Precio',
    'prefijo' => 'Prefijo',
    'plantilla_id' => 'Plantilla',
    'descripcion' => 'Descripcion',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
