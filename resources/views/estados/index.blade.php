@extends('layouts.app')
@section('title')
     @lang('models/estados.plural')
@endsection
@section('content')

@if(session()->has('msj'))                    
                    <script type="text/javascript">
                        swal("Imposible eliminar!", "Existen registros asociados!", "error");
                    </script>
@endif
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/estados.plural')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('estados.create')}}" class="btn btn-primary form-btn">@lang('crud.add_new')<i class="fas fa-plus"></i></a>
            </div>
        </div>
    <div class="section-body">
       <div class="card">
            <div class="card-body">
                @include('estados.table')
            </div>
       </div>
   </div>
    
    </section>
@endsection



