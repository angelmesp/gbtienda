<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateproductosRequest;
use App\Http\Requests\UpdateproductosRequest;
use App\Repositories\productosRepository;
use App\Http\Controllers\AppBaseController;
use App\models\rep_plantillas;
use App\models\plantillas;
use Illuminate\Http\Request;
use Flash;
use Response;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\DB;
//use DB;
use PDF;

class productosController extends AppBaseController
{
    /** @var  productosRepository */
    private $productosRepository;

    public function __construct(productosRepository $productosRepo)
    {
        $this->productosRepository = $productosRepo;
    }

    /**
     * Display a listing of the productos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $productos = $this->productosRepository->all();

        return view('productos.index')
        ->with('productos', $productos);
    }

    /**
     * Show the form for creating a new productos.
     *
     * @return Response
     */
    public function create()
    {
         $plantillas = plantillas::all();
        return view('productos.create', compact("plantillas"));
    }

    public function createDesdeVehiculo($id)
    {
        return view('productos.create')->with('desdevehiculo', $desdevehiculo);;
    }

    /**
     * Store a newly created productos in storage.
     *
     * @param CreateproductosRequest $request
     *
     * @return Response
     */
    public function store(CreateproductosRequest $request)
    {
        //$input = $request->all();
        //$productos = $this->productosRepository->create($input);
        $productos = $this->productosRepository->createProducto($request);

        Flash::success(__('messages.saved', ['model' => __('models/productos.singular')]));

        return redirect(route('productos.index'));
    }

    /**
     * Display the specified productos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productos = $this->productosRepository->find($id);

        if (empty($productos)) {
            Flash::error(__('messages.not_found', ['model' => __('models/productos.singular')]));

            return redirect(route('productos.index'));
        }

        return view('productos.show')->with('productos', $productos);
    }

    /**
     * Show the form for editing the specified productos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productos = $this->productosRepository->find($id);

        if (empty($productos)) {
            Flash::error(__('messages.not_found', ['model' => __('models/productos.singular')]));

            return redirect(route('productos.index'));
        }

        return view('productos.edit', compact("productos", "id"));
    }

    /**
     * Update the specified productos in storage.
     *
     * @param int $id
     * @param UpdateproductosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateproductosRequest $request)
    {
        $productos = $this->productosRepository->find($id);

        if (empty($productos)) {
            Flash::error(__('messages.not_found', ['model' => __('models/productos.singular')]));

            return redirect(route('productos.index'));
        }

        $file = $request->file('imagen');

        if($file){
                //return 'viene imagen';
            $productos = $this->productosRepository->updateProducto($request,$id);
        }else{
                //return 'no viene imagen';
            $productos = $this->productosRepository->update($request->all(), $id);
        }

        

        Flash::success(__('messages.updated', ['model' => __('models/productos.singular')]));

        return redirect(route('productos.index'));
    }

    /**
     * Remove the specified productos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productos = $this->productosRepository->find($id);

        if (empty($productos)) {
            Flash::error(__('messages.not_found', ['model' => __('models/productos.singular')]));

            return redirect(route('productos.index'));
        }

        $this->productosRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/productos.singular')]));

        return redirect(route('productos.index'));
    }

    public static function qr_generate($nombre = '0', $urlimage = '0' ){
       //QrCode::gradient(255, 0, 255, 255, 0, 0, "vertical")->size(200)->generate('http://www.autorey.cl','../public/qrcodes/qrcode'.uniqid().'.svg');
       // return "Hola";
     $hoy = date("Ymd"); 

       //$datosqr = $nombre.'_'.$urlimage;
     $id=DB::select("SHOW TABLE STATUS LIKE 'productos'");
     $next_id=$id[0]->Auto_increment;
     $datosqr = "http://localhost/softbodega/public/productos/".$next_id;
     $nombreqr = $hoy.time().'_'.uniqid().'_id'.$next_id.'.png';
     $ruta =  'qrcodes/'.$nombreqr;

       //return $urlproducto;
     QrCode::format('png')->size(300)->merge('https://www.autorey.cl/wp-content/uploads/logo_AUTOREY_blanco-2.png',.3,true)->generate($datosqr,$ruta);
     return $nombreqr;

 }

 public static function nextid()
 {
   $id=DB::select("SHOW TABLE STATUS LIKE 'productos'");
   $next_id=$id[0]->Auto_increment;
   return $next_id; 
}

public function nextid2()
{         
 $next_id2 =  $this->nextid();
 return $next_id2; 

        //$veh = productos::class->childrenProducto();
        //return $veh;
}
public function getplantilla($id)
{

    $rep_plantillas = rep_plantillas::where("plantilla_id", $id)->first();

    return json_decode($rep_plantillas);
}

public static function imprimir(Request $request)
{            
    $fecha = $request->input("fecha");
    $productos = DB::table('productos')->where('deleted_at', null)->whereDate('created_at', $fecha)->get();
    $fecha = date('Y-m-d');
    $data = compact('productos','fecha');
    $pdf = PDF::loadView('pdf.etiquetasproductos', $data);            
    return $pdf->download($fecha.'_'.time().'_etiAutoRey.pdf');
}
}