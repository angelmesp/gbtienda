<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateplantillasRequest;
use App\Http\Requests\UpdateplantillasRequest;
use App\Repositories\plantillasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class plantillasController extends AppBaseController
{
    /** @var  plantillasRepository */
    private $plantillasRepository;

    public function __construct(plantillasRepository $plantillasRepo)
    {
        $this->plantillasRepository = $plantillasRepo;
    }

    /**
     * Display a listing of the plantillas.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $plantillas = $this->plantillasRepository->all();

        return view('plantillas.index')
            ->with('plantillas', $plantillas);
    }

    /**
     * Show the form for creating a new plantillas.
     *
     * @return Response
     */
    public function create()
    {
        return view('plantillas.create');
    }

    /**
     * Store a newly created plantillas in storage.
     *
     * @param CreateplantillasRequest $request
     *
     * @return Response
     */
    public function store(CreateplantillasRequest $request)
    {
        $input = $request->all();

        $plantillas = $this->plantillasRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/plantillas.singular')]));

        return redirect(route('plantillas.index'));
    }

    /**
     * Display the specified plantillas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $plantillas = $this->plantillasRepository->find($id);

        if (empty($plantillas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/plantillas.singular')]));

            return redirect(route('plantillas.index'));
        }

        return view('plantillas.show')->with('plantillas', $plantillas);
    }

    /**
     * Show the form for editing the specified plantillas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $plantillas = $this->plantillasRepository->find($id);

        if (empty($plantillas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/plantillas.singular')]));

            return redirect(route('plantillas.index'));
        }

        return view('plantillas.edit')->with('plantillas', $plantillas);
    }

    /**
     * Update the specified plantillas in storage.
     *
     * @param int $id
     * @param UpdateplantillasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateplantillasRequest $request)
    {
        $plantillas = $this->plantillasRepository->find($id);

        if (empty($plantillas)) {
            Flash::error(__('messages.not_found', ['model' => __('models/plantillas.singular')]));

            return redirect(route('plantillas.index'));
        }

        $plantillas = $this->plantillasRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/plantillas.singular')]));

        return redirect(route('plantillas.index'));
    }

    /**
     * Remove the specified plantillas from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)    
    {
        try {
                    $plantillas = $this->plantillasRepository->find($id);

                    if (empty($plantillas)) {
                        Flash::error(__('messages.not_found', ['model' => __('models/plantillas.singular')]));

                        return redirect(route('plantillas.index'));
                    }

                    $this->plantillasRepository->delete($id);

                    Flash::success(__('messages.deleted', ['model' => __('models/plantillas.singular')]));

                    return redirect(route('plantillas.index'));
                } catch(\Exception $exception){

                    //dd($exception);
                    //$errormsg = 'No Customer to de!' .$exception->getCode();
                    //return Response::json(['errormsg'=>$errormsg]);
                    return back()->with('msj', 'Existen rejistros asociados');
                   //return redirect(route('vehiculos.index'));
                }
    }
}
