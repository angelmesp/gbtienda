<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', __('models/versiones.fields.nombre').':') !!}
    <p>{{ $versiones->nombre }}</p>
</div>

<!-- Modelo Id Field -->
<div class="form-group">
    {!! Form::label('modelo_id', __('models/versiones.fields.modelo_id').':') !!}
    <p>{{ $versiones->modelo_id }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', __('models/versiones.fields.descripcion').':') !!}
    <p>{{ $versiones->descripcion }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/versiones.fields.created_at').':') !!}
    <p>{{ $versiones->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/versiones.fields.updated_at').':') !!}
    <p>{{ $versiones->updated_at }}</p>
</div>

