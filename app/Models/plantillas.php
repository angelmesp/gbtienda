<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class plantillas
 * @package App\Models
 * @version October 29, 2021, 12:27 pm UTC
 *
 * @property string $nombre
 * @property string $prefijo
 * @property string $descripcion
 */
class plantillas extends Model
{
    //use SoftDeletes;

    use HasFactory;

    public $table = 'plantillas';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'prefijo',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'prefijo' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'prefijo' => 'required'
    ];

    
}
