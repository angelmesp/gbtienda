<?php

return array (
  'singular' => 'Modelo',
  'plural' => 'Modelos',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'marca_id' => 'Marca',
    'descripcion' => 'Descripcion',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
