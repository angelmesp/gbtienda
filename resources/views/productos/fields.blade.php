
<!-- Nombre Field -->
<div class="form-group col-sm-12">
   <select class="form-control" name="plantilla " id="plantilla" style="width: 200px;">
    <option value="">Seleccionar Plantilla</option>
 
    @foreach($plantillas as $rep_plantilla)

        <option value="{{$rep_plantilla->id}}">{{$rep_plantilla->nombre}}</option>
    @endforeach
</select>

</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', __('models/productos.fields.nombre').':') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Precio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('precio', __('models/productos.fields.precio').':') !!}
    {!! Form::text('precio', null, ['class' => 'form-control']) !!}
</div>

<!-- Idvehiculo Field -->
@if(isset($_GET['idvec']))
<div class="form-group col-sm-6">

    {!! Form::label('vehiculo_id', __('models/productos.fields.vehiculo_id').':') !!}            
    {!! Form::select('vehiculo_id', $vehiculoItems, $_GET['idvec'], ['class' => 'form-control']) !!}  
 
    
</div>
@endif

<!-- Idestado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estado_id', __('models/productos.fields.estado_id').':') !!}
    {!! Form::select('estado_id', $estadoItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Idmovilidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('movilidad_id', __('models/productos.fields.movilidad_id').':') !!}
    {!! Form::select('movilidad_id', $movilidadeItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Bodega Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bodega', __('models/productos.fields.bodega').':') !!}
    {!! Form::text('bodega', null, ['class' => 'form-control']) !!}
</div>

<!-- Codrepinter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codrepinter', __('models/productos.fields.codrepinter').':') !!}
    {!! Form::text('codrepinter', null, ['class' => 'form-control']) !!}
</div>

<!-- Codrepfab Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codrepfab', __('models/productos.fields.codrepfab').':') !!}
    {!! Form::text('codrepfab', null, ['class' => 'form-control']) !!}
</div>

<!-- Codigobarra Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigobarra', __('models/productos.fields.codigobarra').':') !!}
    {!! Form::text('codigobarra', null, ['class' => 'form-control']) !!}
</div>

<!-- Stock Field -->
<div class="form-group col-sm-6">
    {!! Form::label('stock', __('models/productos.fields.stock').':') !!}
    {!! Form::text('stock', null, ['class' => 'form-control']) !!}
</div>

<!-- Imagen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('imagen', __('models/productos.fields.imagen').':') !!}
    {!! Form::file('imagen') !!}
</div>
<div class="clearfix"></div>

<!-- Qrcode Field 
<div class="form-group col-sm-6">
    {!! Form::label('qrcode', __('models/productos.fields.qrcode').':') !!}
    {!! Form::file('qrcode') !!}
</div>
<div class="clearfix"></div>

-->

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', __('models/productos.fields.descripcion').':') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('productos.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>



<script>
var plantilla = document.querySelector("#plantilla");

plantilla.addEventListener("change", myScript);
function myScript(){
    var id = this.value;
var url = window.location.origin + "/getplantilla/"+ id;
console.log(url)
fetch(url)
.then((resp) => resp.json())
.then(function(data) {

    document.getElementById("nombre").value = data.nombre
    document.getElementById("precio").value = data.precio

})
.catch(function(error) {
  console.log(error);
});

}



</script>