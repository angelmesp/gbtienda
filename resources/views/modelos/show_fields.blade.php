<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', __('models/modelos.fields.nombre').':') !!}
    <p>{{ $modelos->nombre }}</p>
</div>

<!-- Marca Id Field -->
<div class="form-group">
    {!! Form::label('marca_id', __('models/modelos.fields.marca_id').':') !!}
    <p>{{ $modelos->marca_id }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', __('models/modelos.fields.descripcion').':') !!}
    <p>{{ $modelos->descripcion }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/modelos.fields.created_at').':') !!}
    <p>{{ $modelos->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/modelos.fields.updated_at').':') !!}
    <p>{{ $modelos->updated_at }}</p>
</div>

