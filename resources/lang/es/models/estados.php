<?php

return array (
  'singular' => 'Estado',
  'plural' => 'Estados',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'modelo_id' => 'Modelo',
    'descripcion' => 'Descripcion',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
