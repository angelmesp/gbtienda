<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatevehiculosRequest;
use App\Http\Requests\UpdatevehiculosRequest;
use App\Repositories\vehiculosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class vehiculosController extends AppBaseController
{
    /** @var  vehiculosRepository */
    private $vehiculosRepository;

    public function __construct(vehiculosRepository $vehiculosRepo)
    {
        $this->vehiculosRepository = $vehiculosRepo;
    }

    /**
     * Display a listing of the vehiculos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $vehiculos = $this->vehiculosRepository->all();

        return view('vehiculos.index')
            ->with('vehiculos', $vehiculos);
    }

    /**
     * Show the form for creating a new vehiculos.
     *
     * @return Response
     */
    public function create()
    {
        return view('vehiculos.create');
    }

    /**
     * Store a newly created vehiculos in storage.
     *
     * @param CreatevehiculosRequest $request
     *
     * @return Response
     */
    public function store(CreatevehiculosRequest $request)
    {
        //$input = $request->all();
        //$vehiculos = $this->vehiculosRepository->create($input);
        $vehiculos = $this->vehiculosRepository->createVehiculo($request);

        Flash::success(__('messages.saved', ['model' => __('models/vehiculos.singular')]));

        return redirect(route('vehiculos.index'));
    }

    /**
     * Display the specified vehiculos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $vehiculos = $this->vehiculosRepository->find($id);

        if (empty($vehiculos)) {
            Flash::error(__('messages.not_found', ['model' => __('models/vehiculos.singular')]));

            return redirect(route('vehiculos.index'));
        }

        return view('vehiculos.show')->with('vehiculos', $vehiculos);
    }

    /**
     * Show the form for editing the specified vehiculos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $vehiculos = $this->vehiculosRepository->find($id);

        if (empty($vehiculos)) {
            Flash::error(__('messages.not_found', ['model' => __('models/vehiculos.singular')]));

            return redirect(route('vehiculos.index'));
        }

        return view('vehiculos.edit')->with('vehiculos', $vehiculos);
    }

    /**
     * Update the specified vehiculos in storage.
     *
     * @param int $id
     * @param UpdatevehiculosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatevehiculosRequest $request)
    {
        $vehiculos = $this->vehiculosRepository->find($id);

        if (empty($vehiculos)) {
            Flash::error(__('messages.not_found', ['model' => __('models/vehiculos.singular')]));

            return redirect(route('vehiculos.index'));
        }

        $file = $request->file('imagen');

        if($file){
                //return 'viene imagen';
            $vehiculos = $this->vehiculosRepository->updateVehiculo($request,$id);
        }else{
                //return 'no viene imagen';
            $vehiculos = $this->vehiculosRepository->update($request->all(), $id);
        }

        Flash::success(__('messages.updated', ['model' => __('models/vehiculos.singular')]));

        return redirect(route('vehiculos.index'));
    }

    /**
     * Remove the specified vehiculos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        
        try {
                    $vehiculos = $this->vehiculosRepository->find($id);

                    if (empty($vehiculos)) {
                        Flash::error(__('messages.not_found', ['model' => __('models/vehiculos.singular')]));

                        return redirect(route('vehiculos.index'));
                    }

                    $this->vehiculosRepository->delete($id);

                    Flash::success(__('messages.deleted', ['model' => __('models/vehiculos.singular')]));

                    return redirect(route('vehiculos.index'));

            } catch(\Exception $exception){

                    //dd($exception);
                    //$errormsg = 'No Customer to de!' .$exception->getCode();
                    //return Response::json(['errormsg'=>$errormsg]);
                    return back()->with('msj', 'Existen rejistros asociados');
                   //return redirect(route('vehiculos.index'));
                }



    }
}
