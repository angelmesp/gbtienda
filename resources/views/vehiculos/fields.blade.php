<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', __('models/vehiculos.fields.nombre').':') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Version Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('version_id', __('models/vehiculos.fields.version_id').':') !!}
    {!! Form::select('version_id', $versioneItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Inidesarme Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inidesarme', __('models/vehiculos.fields.inidesarme').':') !!}
    {!! Form::text('inidesarme', null, ['class' => 'form-control']) !!}
</div>

<!-- Preciocompra Field -->
<div class="form-group col-sm-6">
    {!! Form::label('preciocompra', __('models/vehiculos.fields.preciocompra').':') !!}
    {!! Form::text('preciocompra', null, ['class' => 'form-control']) !!}
</div>

<!-- Imagen Field -->
<div class="form-group col-sm-6">
    {!! Form::label('imagen', __('models/vehiculos.fields.imagen').':') !!}
    <input type="file" name="imagen[]" id="imagen" multiple class="form-control">

</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', __('models/vehiculos.fields.descripcion').':') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('vehiculos.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
