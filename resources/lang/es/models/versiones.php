<?php

return array (
  'singular' => 'Version',
  'plural' => 'Versiones',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'modelo_id' => 'Modelo',
    'plantillaId' => 'Plantilla',
    'descripcion' => 'Descripcion',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'codversion' => 'Codigo interno',
    'anoini' => 'Año inicio',
    'anofin' => 'Año fin',
  ),
);
