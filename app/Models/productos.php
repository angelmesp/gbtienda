<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class productos
 * @package App\Models
 * @version November 8, 2021, 6:41 pm UTC
 *
 * @property string $nombre
 * @property integer $precio
 * @property integer $idvehiculo
 * @property integer $idestado
 * @property integer $idmovilidad
 * @property string $bodega
 * @property string $codrepinter
 * @property string $codrepfab
 * @property integer $codigobarra
 * @property integer $stock
 * @property string $imagen
 * @property string $qrcode
 * @property string $descripcion
 */
class productos extends Model
{
    //use SoftDeletes;

    use HasFactory;

    public $table = 'productos';
    

   // protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'precio',
        'vehiculo_id',
        'estado_id',
        'movilidad_id',
        'bodega',
        'codrepinter',
        'codrepfab',
        'codigobarra',
        'stock',
        'imagen',
        'qrcode',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'precio' => 'integer',
        'vehiculo_id' => 'integer',
        'estado_id' => 'integer',
        'movilidad_id' => 'integer',
        'bodega' => 'string',
        'codrepinter' => 'string',
        'codrepfab' => 'string',
        'codigobarra' => 'integer',
        'stock' => 'integer',
        'imagen' => 'string',
        'qrcode' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'precio' => 'required',
        'vehiculo_id' => 'required',
        'estado_id' => 'required',
        'movilidad_id' => 'required',
        'codrepinter' => 'required'
    ];
    
    public function vehiculo()
    {
        return $this->belongsTo(vehiculos::class, 'vehiculo_id');
    }

    public function estado()
    {
        return $this->belongsTo(estados::class, 'estado_id');
    }

    public function movilidad()
    {
        return $this->belongsTo(movilidades::class, 'movilidad_id');
    }     
}