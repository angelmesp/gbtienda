<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();


Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');



Route::resource('marcas', App\Http\Controllers\marcasController::class);


Route::resource('modelos', App\Http\Controllers\modelosController::class);


Route::resource('versiones', App\Http\Controllers\versionesController::class);


Route::resource('estados', App\Http\Controllers\estadosController::class);


Route::resource('movilidades', App\Http\Controllers\movilidadesController::class);


Route::resource('plantillas', App\Http\Controllers\plantillasController::class);


Route::resource('repPlantillas', App\Http\Controllers\rep_plantillasController::class);


Route::resource('vehiculos', App\Http\Controllers\vehiculosController::class);


Route::resource('productos', App\Http\Controllers\productosController::class);

//mostrar desde controlador en public/XXXXX
Route::get('/qr_generate', [App\Http\Controllers\productosController::class, 'qr_generate'])->name('qr_generate');
Route::get('/nextid', [App\Http\Controllers\productosController::class, 'nextid'])->name('nextid');
Route::get('/nextid2', [App\Http\Controllers\productosController::class, 'nextid2'])->name('nextid2');
Route::post('/imprimir', [App\Http\Controllers\productosController::class, 'imprimir'])->name('imprimir');

Route::get('/getplantilla/{id}', [App\Http\Controllers\productosController::class, 'getPlantilla'])->name('getplantilla');

