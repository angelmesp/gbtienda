<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateestadosRequest;
use App\Http\Requests\UpdateestadosRequest;
use App\Repositories\estadosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class estadosController extends AppBaseController
{
    /** @var  estadosRepository */
    private $estadosRepository;

    public function __construct(estadosRepository $estadosRepo)
    {
        $this->estadosRepository = $estadosRepo;
    }

    /**
     * Display a listing of the estados.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $estados = $this->estadosRepository->all();

        return view('estados.index')
            ->with('estados', $estados);
    }

    /**
     * Show the form for creating a new estados.
     *
     * @return Response
     */
    public function create()
    {
        return view('estados.create');
    }

    /**
     * Store a newly created estados in storage.
     *
     * @param CreateestadosRequest $request
     *
     * @return Response
     */
    public function store(CreateestadosRequest $request)
    {
        $input = $request->all();

        $estados = $this->estadosRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/estados.singular')]));

        return redirect(route('estados.index'));
    }

    /**
     * Display the specified estados.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $estados = $this->estadosRepository->find($id);

        if (empty($estados)) {
            Flash::error(__('messages.not_found', ['model' => __('models/estados.singular')]));

            return redirect(route('estados.index'));
        }

        return view('estados.show')->with('estados', $estados);
    }

    /**
     * Show the form for editing the specified estados.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estados = $this->estadosRepository->find($id);

        if (empty($estados)) {
            Flash::error(__('messages.not_found', ['model' => __('models/estados.singular')]));

            return redirect(route('estados.index'));
        }

        return view('estados.edit')->with('estados', $estados);
    }

    /**
     * Update the specified estados in storage.
     *
     * @param int $id
     * @param UpdateestadosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateestadosRequest $request)
    {
        $estados = $this->estadosRepository->find($id);

        if (empty($estados)) {
            Flash::error(__('messages.not_found', ['model' => __('models/estados.singular')]));

            return redirect(route('estados.index'));
        }

        $estados = $this->estadosRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/estados.singular')]));

        return redirect(route('estados.index'));
    }

    /**
     * Remove the specified estados from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
                $estados = $this->estadosRepository->find($id);

                if (empty($estados)) {
                    Flash::error(__('messages.not_found', ['model' => __('models/estados.singular')]));

                    return redirect(route('estados.index'));
                }

                $this->estadosRepository->delete($id);

                Flash::success(__('messages.deleted', ['model' => __('models/estados.singular')]));

                return redirect(route('estados.index'));
                } catch(\Exception $exception){

                //dd($exception);
                //$errormsg = 'No Customer to de!' .$exception->getCode();
                //return Response::json(['errormsg'=>$errormsg]);
                return back()->with('msj', 'Existen rejistros asociados');
               //return redirect(route('vehiculos.index'));
            }
    }
}
