<div class="row">
    <div class="card-header">
        <h4>{{ $vehiculos->nombre }}</h4>                    
        <div align="right">
            {!! Form::open(['route' => ['productos.destroy', $vehiculos->id], 'method' => 'delete']) !!}
               <div class='btn-group'>
                <a href="{!! route('vehiculos.edit', [$vehiculos->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                   {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
               </div>
            {!! Form::close() !!}
        </div>
      </div>
    <div class="col-12 col-md-6 col-lg-6">
        <div class="card">
          <div class="card-body">
            <ul class="list-group">          
                <li class="list-group-item">Version: {{ $vehiculos->version->nombre }}</li>
                <li class="list-group-item">Inicio desarme: {{ $vehiculos->inidesarme }}</li>
                <li class="list-group-item">Precio compra: ${{ number_format($vehiculos->preciocompra, 0, ',', '.') }}</li>
                <li class="list-group-item">Descripcion: {{ $vehiculos->descripcion }}</li>
                <li class="list-group-item">Creado el {{ $vehiculos->created_at }} y actualizado el{{ $vehiculos->updated_at }}</li>

            </ul>
            </ul>
          </div>
        </div>           
    </div>

    <div class="col-12 col-md-6 col-lg-6">
        <div class="card">
          <div class="card-body">
            <ul class="list-group" align="center"> 
                @if(is_array(json_decode($vehiculos->imagen)))
                @foreach(json_decode($vehiculos->imagen) as $image)
                <li class="list-group-item">
                       
                    <img width="240" height="240" src="{{ asset('endesarme/' . $image) }}"></li> 
                    @endforeach
                    @else
                     <li class="list-group-item">
                       
                    <img width="240" height="240" src="{{ asset('endesarme/' . $vehiculos->imagen[0]) }}"></li> 
                    @endif
          </div>
        </div>          
    </div>
</div>


<div class="table-responsive">
    <h4>Repuestos de {{ $vehiculos->nombre }}</h4>
    <div class="section-header-breadcrumb">
        <a href="{{ route('productos.create', ['idvec'=>$vehiculos->id])}}" class="btn btn-primary form-btn">Craer nuevo repuesto <i class="fas fa-plus"></i></a>
    </div>
    <br>
    <table class="table" id="productos-table">
        <thead>
            <tr>
                <th>@lang('models/productos.fields.nombre')</th>
        <th>@lang('models/productos.fields.precio')</th>
        <th>@lang('models/productos.fields.estado_id')</th>
        <th>@lang('models/productos.fields.movilidad_id')</th>
        <th>@lang('models/productos.fields.bodega')</th>
        <th>@lang('models/productos.fields.codrepinter')</th>
        <!-- <th>@lang('models/productos.fields.codrepfab')</th> -->
        <!-- <th>@lang('models/productos.fields.codigobarra')</th> -->
        <th>@lang('models/productos.fields.stock')</th>
        <th>@lang('models/productos.fields.imagen')</th>
        <!-- <th>@lang('models/productos.fields.qrcode')</th> -->
        <!-- <th>@lang('models/productos.fields.descripcion')</th> -->
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($vehiculos->productos as $productos)
            <tr>
            <td><a href="{!! route('productos.show', [$productos->id]) !!}">{{ $productos->nombre }}</a></td>
            <td>${{ number_format($productos->precio, 0, ',', '.') }}</td>
            <td>{{ $productos->estado->nombre }}</td>
            <td>
                 @switch($productos->movilidad_id)
                        @case(2)
                            <a href='#' class="badge badge-warning">{{ $productos->movilidad->nombre }}</a>                            
                            @break
                        @case(3)
                            <a href='#' class="badge badge-success">{{ $productos->movilidad->nombre }}</a>                          
                          @break
                        @default
                            <a href='#' class="badge badge-light">{{ $productos->movilidad->nombre }}</a>                          
                        @break
                @endswitch
            </td>
            <td>{{ $productos->bodega }}</td>
            <td>{{ $productos->codrepinter }}</td>
            <!-- <td>{{ $productos->codrepfab }}</td> -->
            <!-- <td>{{ $productos->codigobarra }}</td> -->
            <td>{{ $productos->stock }}</td>
            <td><img height="50" src="{{ asset('upload/' . $productos->imagen) }}"></td>
            <!-- <td>{{ $productos->qrcode }}</td> -->
            <!-- <td>{{ $productos->descripcion }}</td> -->
                       <td class=" text-center">
                           {!! Form::open(['route' => ['productos.destroy', $productos->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('productos.show', [$productos->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('productos.edit', [$productos->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>