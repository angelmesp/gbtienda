<?php

namespace App\Repositories;

use App\Models\rep_plantillas;
use App\Repositories\BaseRepository;

/**
 * Class rep_plantillasRepository
 * @package App\Repositories
 * @version October 29, 2021, 12:31 pm UTC
*/

class rep_plantillasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'precio',
        'plantilla_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return rep_plantillas::class;
    }
}
