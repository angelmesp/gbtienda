<?php

namespace App\Repositories;

use App\Models\versiones;
use App\Repositories\BaseRepository;

/**
 * Class versionesRepository
 * @package App\Repositories
 * @version October 18, 2021, 11:41 pm UTC
*/

class versionesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'modelo_id',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return versiones::class;
    }
}
