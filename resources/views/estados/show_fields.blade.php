<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', __('models/estados.fields.nombre').':') !!}
    <p>{{ $estados->nombre }}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', __('models/estados.fields.descripcion').':') !!}
    <p>{{ $estados->descripcion }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/estados.fields.created_at').':') !!}
    <p>{{ $estados->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/estados.fields.updated_at').':') !!}
    <p>{{ $estados->updated_at }}</p>
</div>

