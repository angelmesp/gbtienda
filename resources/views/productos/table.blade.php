<div class="table-responsive">
    <table class="table" id="productos-table">
        <thead>
            <tr>
                <th>@lang('models/productos.fields.nombre')</th>
        <th>@lang('models/productos.fields.precio')</th>
        <th>@lang('models/productos.fields.vehiculo_id')</th>
        <th>@lang('models/productos.fields.estado_id')</th>
        <th>@lang('models/productos.fields.movilidad_id')</th>
        <th>@lang('models/productos.fields.bodega')</th>
        <th>@lang('models/productos.fields.codrepinter')</th>
       
        <th>@lang('models/productos.fields.stock')</th>
        <th>@lang('models/productos.fields.imagen')</th>
       
                <th >@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($productos as $productos)
            <tr>
            <td><a href="{!! route('productos.show', [$productos->id]) !!}">{{ $productos->nombre }}</a></td>
            <td>${{ number_format($productos->precio, 0, ',', '.') }}</td>
            <td><a href="{!! route('vehiculos.show', [$productos->vehiculo_id]) !!}">{{ $productos->vehiculo->nombre }}</a></td>
            <td>{{ $productos->estado->nombre }}</td>
            <td>
                 @switch($productos->movilidad_id)
                        @case(2)
                            <a href='#' class="badge badge-warning">{{ $productos->movilidad->nombre }}</a>                            
                            @break
                        @case(3)
                            <a href='#' class="badge badge-success">{{ $productos->movilidad->nombre }}</a>                          
                          @break
                        @default
                            <a href='#' class="badge badge-light">{{ $productos->movilidad->nombre }}</a>                          
                        @break
                @endswitch
            </td>
            <td>{{ $productos->bodega }}</td>
            <td>{{ $productos->codrepinter }}</td>
            <!-- <td>{{ $productos->codrepfab }}</td> -->
            <!-- <td>{{ $productos->codigobarra }}</td> -->
            <td>{{ $productos->stock }}</td>
            <td><img height="50" src="{{ asset('upload/' . $productos->imagen) }}"></td>
            <!-- <td>{{ $productos->qrcode }}</td> -->
            <!-- <td>{{ $productos->descripcion }}</td> -->
                       <td class=" text-center">
                           {!! Form::open(['route' => ['productos.destroy', $productos->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('productos.show', [$productos->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('productos.edit', [$productos->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>


<!--Data Table-->
<script type="text/javascript"  src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js" defer></script>


<script>
   $(document).ready( function () {

    $('#productos-table').DataTable();
} );
</script>