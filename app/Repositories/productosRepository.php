<?php

namespace App\Repositories;

use App\Models\productos;
use App\Repositories\BaseRepository;
// Uso imagen
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
// uso imagen fin
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use \App\Http\Controllers\productosController;

use DB;
use Str;

/**
 * Class productosRepository
 * @package App\Repositories
 * @version November 8, 2021, 6:41 pm UTC
*/

class productosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'precio',
        'idvehiculo',
        'idestado',
        'idmovilidad',
        'bodega',
        'codrepinter',
        'codrepfab',
        'codigobarra',
        'stock',
        'imagen',
        'qrcode',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return productos::class;
    }

    public function createProducto(Request $request){
        // Creamos esta funcion para intervenir las imagenes
        $file = $request->file('imagen');
        $originalName = $file->getClientOriginalName();
        $extantion = $file->getCLientoriginalExtension();

        $hoy = date("Ymd");  

        $pathimage = $hoy.time().'_'.uniqid().'.'.$extantion;
        $path = 'upload/'.$pathimage;
        $img = Image::make($file)->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            })->insert('img/Marca_desarmaduria_autorey_camionetas.png');
        $img->save(public_path($path));

        // Intervenimos QRcode
        //$qrcode = $request->file('qrcode');
        //$originalNameqrcode = $qrcode->getClientOriginalName();
        //$extantionqrcode = $qrcode->getCLientoriginalExtension();

        $input = $request->all();
        $pathqrcode = productosController::qr_generate($input['nombre'],$input['imagen']);

        //Creamos codigo de prod interno
        $id=DB::select("SHOW TABLE STATUS LIKE 'productos'");
        $next_id=$id[0]->Auto_increment;
        $nombre = strtoupper($input['nombre']);   
        $cod = explode(" ", $nombre);

        //$codvehiculo = BD::select('SELECT versiones.codversion as nombreversion FROM versiones INNER JOIN vehiculos ON vehiculos.version_id = versiones.id where vehiculos.id = 5');

        $vehiculo_id = $input['vehiculo_id'];
        $codvehiculo = DB::table('versiones')
            ->join('vehiculos', 'vehiculos.version_id', '=', 'versiones.id')
            ->select('versiones.codversion as nombrever')
            ->where('vehiculos.id', '=', $request->vehiculo_id )
            ->get();
        //return $codvehiculo;
        // preg_replace deja solo numeros y letras sin caracteres especiales
        $cod1 = preg_replace('([^A-Za-z0-9])', '', $cod[0]); 
        $codnew = "";
        foreach($codvehiculo as $codvehiculo1){
                    $codnew = $codvehiculo1->nombrever;
                }       
        $input['codrepinter'] = $codnew.'-'.$next_id.Str::limit($cod1,1,'');


           
        $input['imagen'] = $pathimage;
        $input['qrcode'] = $pathqrcode;           

        return $this->create($input);

        $input = $request->all();


    }

    public function updateProducto(Request $request, $id){
       // return 'editando repuesto updateRepuesto';
        // Creamos esta funcion para intervenir las imagenes
        $file = $request->file('imagen');
        $originalName = $file->getClientOriginalName();
        $extantion = $file->getCLientoriginalExtension();

        $hoy = date("Ymd");  

        $pathimage = $hoy.time().'_'.uniqid().'.'.$extantion;
        $path = 'upload/'.$pathimage;
        $img = Image::make($file)->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            })->insert('img/Marca_desarmaduria_autorey_camionetas.png');;
        $img->save(public_path($path));

        
        $input = $request->all();
        //$pathqrcode = repuestoController::qr_generate($input['nombre'],$input['imagen']);

       
        $input['imagen'] = $pathimage;
        //$input['qrcode'] = $pathqrcode;           

        return $this->update($input, $id);


    } 
}