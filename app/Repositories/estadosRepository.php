<?php

namespace App\Repositories;

use App\Models\estados;
use App\Repositories\BaseRepository;

/**
 * Class estadosRepository
 * @package App\Repositories
 * @version October 19, 2021, 7:39 pm UTC
*/

class estadosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return estados::class;
    }
}
