<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class vehiculos
 * @package App\Models
 * @version November 4, 2021, 7:39 pm UTC
 *
 * @property string $nombre
 * @property integer $version_id
 * @property integer $inidesarme
 * @property integer $preciocompra
    * @property string $imagen
 * @property string $descripcion
 */
class vehiculos extends Model
{
    //use SoftDeletes;

    use HasFactory;

    public $table = 'vehiculos';
    

   // protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'version_id',
        'inidesarme',
        'preciocompra',
        'imagen',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'version_id' => 'integer',
        'inidesarme' => 'integer',
        'preciocompra' => 'integer',
        'imagen' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'version_id' => 'required',
        'inidesarme' => 'required',
        'preciocompra' => 'numeric'
    ];

    public function version()
    {
        return $this->belongsTo(versiones::class, 'version_id');
    }   

    // una vehiculo tiene muchos productos
    public function productos()
    {
        return $this->hasMany(productos::class, 'vehiculo_id');
    } 
}