<div class="table-responsive">
    <table class="table" id="movilidades-table">
        <thead>
            <tr>
                <th>@lang('models/movilidades.fields.nombre')</th>
        <th>@lang('models/movilidades.fields.descripcion')</th>
                <th colspan="3">@lang('crud.action')</th>
            </tr>
        </thead>
        <tbody>
        @foreach($movilidades as $movilidades)
            <tr>
                       <td>{{ $movilidades->nombre }}</td>
            <td>{{ $movilidades->descripcion }}</td>
                       <td class=" text-center">
                           {!! Form::open(['route' => ['movilidades.destroy', $movilidades->id], 'method' => 'delete']) !!}
                           <div class='btn-group'>
                               <a href="{!! route('movilidades.show', [$movilidades->id]) !!}" class='btn btn-light action-btn '><i class="fa fa-eye"></i></a>
                               <a href="{!! route('movilidades.edit', [$movilidades->id]) !!}" class='btn btn-warning action-btn edit-btn'><i class="fa fa-edit"></i></a>
                               {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger action-btn delete-btn', 'onclick' => 'return confirm("'.__('crud.are_you_sure').'")']) !!}
                           </div>
                           {!! Form::close() !!}
                       </td>
                   </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
