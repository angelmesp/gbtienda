<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateversionesRequest;
use App\Http\Requests\UpdateversionesRequest;
use App\Repositories\versionesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use DB;
use Str;

class versionesController extends AppBaseController
{
    /** @var  versionesRepository */
    private $versionesRepository;

    public function __construct(versionesRepository $versionesRepo)
    {
        $this->versionesRepository = $versionesRepo;
    }

    /**
     * Display a listing of the versiones.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $versiones = $this->versionesRepository->all();

        return view('versiones.index')
            ->with('versiones', $versiones);
    }

    /**
     * Show the form for creating a new versiones.
     *
     * @return Response
     */
    public function create()
    {
        return view('versiones.create');
    }

    /**
     * Store a newly created versiones in storage.
     *
     * @param CreateversionesRequest $request
     *
     * @return Response
     */
    public function store(CreateversionesRequest $request)
    {
        $input = $request->all();

        //Creamos codigo de version
        $id=DB::select("SHOW TABLE STATUS LIKE 'versiones'");
        $next_id=$id[0]->Auto_increment;
        $nombre = strtoupper($input['nombre']);   
        $cod = explode(" ", $nombre);
        // preg_replace deja solo numeros y letras sin caracteres especiales
        $cod1 = preg_replace('([^A-Za-z0-9])', '', $cod[0]);
        if(count($cod)==1){
        $input['codversion'] = $next_id.Str::limit($cod1,2,'');
        }else{
        $cod2 =  preg_replace('([^A-Za-z0-9])', '', $cod[1]); //piece2
        $input['codversion'] = $next_id.'-'.Str::limit($cod1,2,'').Str::limit($cod2,2,'');
        }
        

        $versiones = $this->versionesRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/versiones.singular')]));

        return redirect(route('versiones.index'));
    }

    /**
     * Display the specified versiones.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $versiones = $this->versionesRepository->find($id);

        if (empty($versiones)) {
            Flash::error(__('messages.not_found', ['model' => __('models/versiones.singular')]));

            return redirect(route('versiones.index'));
        }

        return view('versiones.show')->with('versiones', $versiones);
    }

    /**
     * Show the form for editing the specified versiones.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $versiones = $this->versionesRepository->find($id);

        if (empty($versiones)) {
            Flash::error(__('messages.not_found', ['model' => __('models/versiones.singular')]));

            return redirect(route('versiones.index'));
        }

        return view('versiones.edit')->with('versiones', $versiones);
    }

    /**
     * Update the specified versiones in storage.
     *
     * @param int $id
     * @param UpdateversionesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateversionesRequest $request)
    {
        $versiones = $this->versionesRepository->find($id);

        if (empty($versiones)) {
            Flash::error(__('messages.not_found', ['model' => __('models/versiones.singular')]));

            return redirect(route('versiones.index'));
        }

        $versiones = $this->versionesRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/versiones.singular')]));

        return redirect(route('versiones.index'));
    }

    /**
     * Remove the specified versiones from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
                $versiones = $this->versionesRepository->find($id);

                if (empty($versiones)) {
                    Flash::error(__('messages.not_found', ['model' => __('models/versiones.singular')]));

                    return redirect(route('versiones.index'));
                }

                $this->versionesRepository->delete($id);

                Flash::success(__('messages.deleted', ['model' => __('models/versiones.singular')]));

                return redirect(route('versiones.index'));
        } catch(\Exception $exception){

            //dd($exception);
            //$errormsg = 'No Customer to de!' .$exception->getCode();
            //return Response::json(['errormsg'=>$errormsg]);
            return back()->with('msj', 'Existen rejistros asociados');
           //return redirect(route('vehiculos.index'));
        }
    }
}