<?php

namespace App\Repositories;

use App\Models\movilidades;
use App\Repositories\BaseRepository;

/**
 * Class movilidadesRepository
 * @package App\Repositories
 * @version October 19, 2021, 7:49 pm UTC
*/

class movilidadesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return movilidades::class;
    }
}
