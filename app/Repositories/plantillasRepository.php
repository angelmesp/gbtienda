<?php

namespace App\Repositories;

use App\Models\plantillas;
use App\Repositories\BaseRepository;

/**
 * Class plantillasRepository
 * @package App\Repositories
 * @version October 29, 2021, 12:27 pm UTC
*/

class plantillasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'prefijo',
        'descripcion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return plantillas::class;
    }
}
