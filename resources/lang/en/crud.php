<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'back' => 'Atras',
    'add_new' => 'Crear ',
    'save' => 'Guardar',
    'cancel' => 'Cancelar',
    'action' => 'Acciones ',
    'edit' => 'Editar ',
    'details' => 'Detalles',
    'are_you_sure' => '¿Estas seguro?',

];
