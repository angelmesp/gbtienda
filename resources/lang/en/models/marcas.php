<?php

return array (
  'singular' => 'marcas',
  'plural' => 'marcas',
  'fields' => 
  array (
    'id' => 'Id',
    'nombre' => 'Nombre',
    'descripcion' => 'Descripcion',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
